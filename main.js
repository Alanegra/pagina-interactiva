// Carga ficheros con w3.js
includeHTML();

  // Menu chetado -->

function openNav() {
  document.getElementById("myNav").style.width = "100%";
}

function closeNav() {
  document.getElementById("myNav").style.width = "0%";
}

// Boton Experimental Ejercicio1
function myFunction() {
  var letter = document.getElementById("Ej1").value;
  var letter2 = document.getElementById("Ej1-2").value;

  if (letter === "jquery" && letter2 === "javascript") {
    text = "Amazing, puedes pasar al Ejercicio2 adelante. ";
    document.getElementById("Ej1").style.border = "4px solid green";
    document.getElementById("Ej1").style.backgroundColor = "lightgreen";
    document.getElementById("Ej1-2").style.border = "4px solid green";
    document.getElementById("Ej1-2").style.backgroundColor = "lightgreen";
    document.getElementById("demo").style.color = "lightgreen";
    document.getElementById("demo").style.backgroundColor = "black";
  //  $("#Ej1").animate({borderColor: "green"});
  //  $("#Ej1").animate({backgroundColor: "#33cc33"});
  } else {
    text = "Con que pongas una palabra incorrecta ya esta mal, tienen que estar todas bien.";
    document.getElementById("Ej1").style.border = "4px solid darkred";
    document.getElementById("Ej1").style.backgroundColor = "red";
    document.getElementById("Ej1-2").style.border = "4px solid darkred";
    document.getElementById("Ej1-2").style.backgroundColor = "red";
    document.getElementById("demo").style.color = "red";
    document.getElementById("demo").style.backgroundColor = "black";
  //  $("#Ej1").animate({borderColor: "red"});
  //  $("#Ej1").animate({backgroundColor: "red"});
  }
  document.getElementById("demo").innerHTML = text;
}
// Boton Experimental Ejercicio2
function myFunction2() {
  var letter = document.getElementById("Ej2").value;
  if (letter === "utf-8") {
    text = "Amazing, puedes pasar al Ejercicio3 adelante. ";
    document.getElementById("Ej2").style.border = "4px solid green";
    document.getElementById("Ej2").style.backgroundColor = "lightgreen";
    document.getElementById("demo").style.color = "lightgreen";
    document.getElementById("demo").style.backgroundColor = "black";
  } else {
    text = "Incorrecto.";
    document.getElementById("Ej2").style.border = "4px solid darkred";
    document.getElementById("Ej2").style.backgroundColor = "red";
    document.getElementById("demo").style.color = "red";
    document.getElementById("demo").style.backgroundColor = "black";
  }
  document.getElementById("demo").innerHTML = text;
}

// Boton Experimental Ejercicio3
function myFunction3() {
  var image = document.getElementById('myImage');
  var image2 = document.getElementById('myImage2');
  var image3 = document.getElementById('myImage3');

  if (image3.src.match("bulbon1")) {

    text = "Amazing, puedes pasar al Ejercicio4 adelante. ";
    document.getElementById('myImage').style.backgroundColor = "lightgreen";
    document.getElementById('myImage').style.border = "4px solid green";
    document.getElementById('myImage2').style.backgroundColor = "lightgreen";
    document.getElementById('myImage2').style.border = "4px solid green";
    document.getElementById('myImage3').style.backgroundColor = "lightgreen";
    document.getElementById('myImage3').style.border = "4px solid green";
  } else {
    text = "Incorrecto.";
    document.getElementById('myImage').style.backgroundColor = "red";
    document.getElementById('myImage').style.border = "4px solid darkred";
    document.getElementById('myImage2').style.backgroundColor = "red";
    document.getElementById('myImage2').style.border = "4px solid darkred";
    document.getElementById('myImage3').style.backgroundColor = "red";
    document.getElementById('myImage3').style.border = "4px solid darkred";
  }
  document.getElementById("demo").innerHTML = text;
}

function changeImage() {
  var image = document.getElementById('myImage');
  if (image.src.match("bulbon")) {
    image.src = "pic_bulboff.gif";
  } else {
    image.src = "pic_bulbon.gif";
  }
}
function changeImage2() {
  var image2 = document.getElementById('myImage2');
  if (image2.src.match("bulbon0")) {
    image2.src = "pic_bulboff0.gif";
  } else {
    image2.src = "pic_bulbon0.gif";
  }
}
function changeImage3() {
  var image3 = document.getElementById('myImage3');
  if (image3.src.match("bulbon1")) {
    image3.src = "pic_bulboff1.gif";
  } else {
    image3.src = "pic_bulbon1.gif";
  }
}



// Cambio de Imagen -->
var slideIndex = 0;
showSlides();

function showSlides() {
var i;
var slides = document.getElementsByClassName("mySlides");
var dots = document.getElementsByClassName("dot");
for (i = 0; i < slides.length; i++) {
  slides[i].style.display = "none";
}
slideIndex++;
if (slideIndex > slides.length) {slideIndex = 1}
for (i = 0; i < dots.length; i++) {
  dots[i].className = dots[i].className.replace(" active", "");
}
//slides[slideIndex-1].style.display = "block";
// dots[slideIndex-1].className += " active";
setTimeout(showSlides, 2000); // Change image every 2 seconds
}




$(document).ready(function(){
  console.log("Welcome to examples");

  $('.draggable').draggable({
    start: function () {
        $(this).animate({
          rotate: "-720deg",
          width: "10px",
          height: "10px"
        }, 1000);
    },
    stop: function () {
        $(this).animate({
          rotate: "360deg",
          width: "100px",
          height: "50px"
        }, 1000);
    }
  });

  $('.droppable').droppable({
      drop: function () {
          $('#draggable').animate({
              opacity: 0.5
          }, 1000);//.remove();
          $(this).html('Dropped');
      }
  });


 // FIN EJEMPLO 2
  // INICIO EJEMPLO 6
  $("#show-content").on("click", function() {

});

$(".drag").draggable({
   cursor: 'move',
   revert: true,
   helper: 'clone',
   start: function () {
       $(this).animate({
         rotate: "-720deg",
         width: "10px",
         height: "10px",
         backgroundColor: "blue"
       }, 1000);
   },
   stop: function () {
       $(this).animate({
         rotate: "360deg",
         width: "100px",
         height: "50px"
       }, 1000);
   }
});

$(".drop").droppable({
  accept: '.drag',
  drop: function(event, ui) {
            console.log(ui.draggable.prop("id") + " dropped onto " + $(this).prop("id"));
          }


    // FIN EJEMPLO 6
 });

});
